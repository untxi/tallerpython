## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##
##   Módulo: Métodos_Ordenamiento.py   ##
##                                     ##
## Métodos:                            ##
##      ~ Burbuja                      ##
##      ~ Heap Sort                    ##
##      ~ Inserción                    ##
##      ~ Quick Sort                   ##
##      ~ Selección                    ##
##      ~ Shell                        ##
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-. IMPORTACIÓN DE BILBIOTECAS .-.  ##
import math
from time import *
import time
import timeit
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-. Calculo de Tiempo .-.-.-.-. ##
##print("---------------TIEMPO--------------------")
#### timeit.Timer("función()","from __main__ import función")
##t= timeit.Timer("burbuja()","from __main__ import burbuja")
##seg=(t.timeit(1))
##mili= seg*1000
##print(mili)
##print(t.timeit(1))
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-.- Método  Burbuja .-.-.-.-. ##
def burbuja():
    ## PEGUE LA LISTA POR ORDENAR
    ##l =

    ## Contadores
    pasos = 0
    cambios = 0
    
    for pasada in range(1, len(l)-1):
        pasos += 1
    for i in range(0,len(l)-1): 
        if l[i] > l[i+1]:
          cambios += 1
          l[i], l[i+1] = l[i+1], l[i]
          
    print("Pasos",pasos,"cambios",cambios)
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-.-. Método HeapSort .-.-.-.-. ##
def HeapSort():
    ## PEGUE LA LISTA ##
    ##sqc =

    ## Contadores
    interC = 0
    Compa = 0
    
    def swap(i, j):
        interC += 1
        sqc[i], sqc[j] = sqc[j], sqc[i] 

    def heapify(end,i):
        l = 2 * i + 1  
        r = 2 * (i + 1)   
        max = i
        Compa += 1 ### COMPARA
        if l < end and sqc[i] < sqc[l]:   
            max = l
        Compa += 1 ### COMPARA
        if r < end and sqc[max] < sqc[r]:   
            max = r   
        if max != i:   
            swap(i, max)   
            heapify(end, max)   

    def heap_sort():
        end = len(sqc)
        start = end / 2 - 1
        for i in range(int(start), -1, -1):
            heapify(end, i)
        for i in range(end-1, 0, -1):
            swap(i, 0)
            heapify(i, 0)

    heap_sort()
    print("Compara", Compa, "Intercambia", interC)
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-.-. Método Inserción .-.-.-.-.##
def Insercion():
    ## PEGUE LA LISTA ##
    ##l =

    ## Contadores
    pasos=0
    cambios=0
    
    for i in range(1,len(l)) :
      pasos+=1
      aux=l[i] 
      j=i 
      while j>0 and aux<l[j-1] :
        cambios+=1
        l[j]=l[j-1] 
        j-=1 
      l[j]= aux
      
    print("Pasos",pasos,"Cambios",cambios)
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-.-. Método QuickSort .-.-.-.-.##
def QuickSort():

    ## Contadores
    inter = 0
    com = 0
    
    def quicksort () :
        ## PEGUE LA LISTA ##
        ##l =
        ordena_quicksort(lista,0,len(lista)-1) 
        return lista 
     
    def ordena_quicksort (lista,izdo,dcho) :
        global inter, com
        if izdo<dcho :
            pivote=lista[math.floor((izdo+dcho)/2)] 
            i,d=izdo,dcho 
            while i<=d : 
                while lista[i]<pivote :
                    com += 1
                    i+=1 
                while lista[d]>pivote :
                    com +=1
                    d-=1 
                if i<=d :
                    inter +=1
                    lista[i],lista[d]=lista[d],lista[i] 
                    i+=1 
                    d-=1 
            if izdo<d : ordena_quicksort(lista,izdo,d) 
            if i<dcho : ordena_quicksort(lista,i,dcho) 
        return lista

    quicksort()
    print(len(l), "Compara ", com, "Intercambia ", inter)
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-.-. Método Selección .-.-.-.-.##
def Seleccion():
    ## PEGUE LA LISTA ##
    ##l =
    
    ## Contadores
    pasadas=0
    cambio=0
    
    for i in range(0, len(l)-1) :
      pasadas+=1
      indiceMenor=i 
      for j in range(i+1, len(l)) :
        cambio+=1
        if l[j]<l[indiceMenor] : 
          indiceMenor=j 
      if i!=indiceMenor : 
        l[i],l[indiceMenor]=l[indiceMenor],l[i]
        
    print("Pass",pasadas,"Change",cambio)
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##

## .-.-.-.-. Método Shell Sort .-.-.-. ##
def shellsort () :
    ## PEGUE LA LISTA ##
        ##l =
 
    ## Contadores
    inter = 0
    compa = 0
  
    incremento = math.floor(len(lista)/2)
    while (incremento>0) :
    
      for i in range(incremento,len(lista)) : 
        j=i 
        temporal=lista[i]
        compa += 1 ##### COMPARACIONES #####
        while ( (j>=incremento) and (lista[j-incremento]>temporal) ) :
            inter += 1 #### INTERCAMBIOS ####
            lista[j]=lista[j-incremento] 
            j=j-incremento 
        lista[j]=temporal
      
      if (incremento==2) :
          incremento=1 
      else :
          incremento=int(incremento/2.2)
      
    print("Compara", compa, "Intercambia", inter)
    return lista
## .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-. ##
