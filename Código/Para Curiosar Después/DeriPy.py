# -*- coding: utf-8 -*- 
"""
########################################################################
# Programa: CalculoDerivada.py                                         #
# Autores :                                                            #
#         Samantha Arburola Leon   2013101697                          #
#         Samantha Mendoza Peralta 2013016264                          #
#         Amanda Solano Astorga    20132000                            #
# Fecha   : 08.abril.2015                                              #
#                                                                      #
#         Este programa grafica cualquier funcion simbolica, su Recta  #  
# Tangente y  Recta Normal en un punto determinado.  La funcion y el   #
# punto 'x' para analizar son ingresados por el usuario.               #
#                                                                      #
#         La interaccion humano-programa es interfaz grafica sencilla  #
# y eficiente con:                                                     #
#    (a)Botones para acceder y retroceder en las ventanas que muestran #
#       las gráficas al acercar o alejar la funcion                    #
#    (b)Instrucciones visibles durante la insercion de datos y         #
#       observacion de  la grafica                                     #
#																	   #
########################################################################
"""
""" ---------------------------------------------------------------- """
########################################################################
""" ---------------------------------------------------------------- """
""" --------------------- Programa: Librerias ---------------------- """
## Libreria Grafica
import Tkinter
from Tkinter import *   
from Tkinter import Tk  
## Libreria Matematica
from sympy import *
## Libreria para trabajar con el str ingresado por el usuario 
from sympy.parsing.sympy_parser import parse_expr
## Libreria para graficar las funciones
from matplotlib import *
import matplotlib.pyplot as plt
""" ---------------------------------------------------------------- """
########################################################################
""" --------------------- Programa: Matematico --------------------- """
## Funcion: Error.  
def error():
        """ Alertar si se presenta un error en la digitacion de datos
		Entrada: Ninguna.
		Salida : Ninguna.
		Restriccion: Ninguna.
	"""
	## Creacion de la ventana hija de la principal y asignacion de Propiedades
        error = Tkinter.Toplevel(root)	## Herencia
        error.title("ERROR")
        error.geometry("200x150")
        error.config(bg = "#FFFFCC")
        ## Mensaje de Error
        mensaje = Tkinter.Label(error, text="ERROR\n\nReingrese Datos\nSegún las instrucciones", justify=CENTER)
        mensaje.config(bg = "#FFFFCC", fg = "black")
        mensaje.place(x = 25, y = 30)
        ## Boton Cerrar Ventana
        bttn_cerrar = Tkinter.Button(error, text= "Cerrar", relief = "raised",command = lambda:error.destroy())
        bttn_cerrar.config(bg = "#CC99FF", fg = "black")
        bttn_cerrar.place(x=70,y=100)
        #
        error.transient(root)
    
########################################
### Definicon de Simbolos y globales ###
x = Symbol('x')
normal = ""
miFuncion=""
miPunto=""
y=0
m=0
########################################
###       Obtener   Punto y          ###
def Obtener_y():
    global y
    try:
        y=miFuncion.evalf(subs={x:miPunto})
        y= y.evalf(2)
    except:
        error()
        instructivo()
        derivarIN()
########################################
###  Sacar  Derivada de una funcion  ###
def Sacar_Derivada():
    global derivada
    try:
        derivada= ((diff(miFuncion,x)))
    except:
        error()
        instructivo()
        derivarIN()
########################################
###   Sacar Pendiente de una recta   ###
def  Sacar_m():
    global m
    try:
        m= ((derivada.evalf(subs={x:miPunto})))
        m = (m.evalf(2))
    except:
        error()
        instructivo()
        derivarIN()
########################################
###     Obtener  la Recta Tangente   ###
def obtener_recta_tangente():
    global tangente
    try:
        b = -(m*miPunto) + y
        tangente=  (m*x +b)
    except:
        error()
        instructivo()
        derivarIN()
########################################
###     Obtener  la Recta Normal     ###
def obtener_recta_normal():
    global normal
    try:
        inverso = -(Rational(m) ** -1)
        b = -(inverso * miPunto) + Rational(y)
        global normal
        normal =(inverso*x + b)
    except:
        error()
        instructivo()
        derivarIN()
""" ---------------------------------------------------------------- """
########################################################################
""" ---------------------------------------------------------------- """
""" --------------------- Programa: Interfaz ----------------------- """ 
########################################
### Ingresar los datos para derivar  ###   
def derivarIN():
    def fillData():
        global miFuncion
        global miPunto
        miFuncion  = miEntradaF.get()       
        miPunto    = miEntradaP.get()
        miFuncion = parse_expr(str(miFuncion))
        miPunto = float(miPunto)
        derivar_win.withdraw()
        derivarOUT()
    """ FIN fillData() """
      
    ## Crea la ventana Tk
    global derivar_win
    derivar_win = Tkinter.Toplevel(root)
    derivar_win.config(bg = "#FFFFCC")
    derivar_win.geometry("350x130+200+200") ## Ancho, Largo, X Y (posiciona ventana pantalla)
    derivar_win.title("DeriPy - Ingresar Datos")
    ## Bienvenida
    bienvenida = Tkinter.Label(derivar_win, text = "Ingreso de Datos", font = "Century 12", justify = CENTER)
    bienvenida.config(bg = "#FFFFCC", fg = "black")
    bienvenida.place(x = 100, y = 10)
    ## Ingresar Funcion
    funcion = Tkinter.Label(derivar_win, text = "Funcion", font = "Century 9", justify = CENTER)
    funcion.config(bg = "#FFFFCC", fg = "black")
    funcion.place(x = 10, y = 30)
    ## Cuandro para ingresar txt
    global miEntradaF
    txtFuncion = Tkinter.StringVar()
    miEntradaF = Tkinter.Entry(derivar_win,textvariable = txtFuncion, width=40)
    miEntradaF.place(x = 10, y = 50)
    ## Ingresar Punto
    punto = Tkinter.Label(derivar_win, text = "Punto x", font = "Century 9", justify = CENTER)
    punto.config(bg = "#FFFFCC", fg = "black")
    punto.place(x = 10, y = 73)
    ## Cuandro para ingresar txt
    global miEntradaP
    txtPunto = Tkinter.StringVar()
    miEntradaP = Tkinter.Entry(derivar_win,textvariable = txtPunto, width=15)
    miEntradaP.place(x = 10, y = 93)
    ## Boton Derivar
    bttn_cerrar = Tkinter.Button(derivar_win, text= "- Derivar -", relief = "raised",command = fillData)
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x=160,y=90)
    ## Boton Cerrar Ventana
    bttn_cerrar = Tkinter.Button(derivar_win, text= "- Cerrar  -", relief = "raised",command = lambda:derivar_win.destroy())
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x=250,y=90)
    derivar_win.transient(root)
    """ ====================== FIN derivarIN ======================= """
########################################
###      Graficar las Funciones      ###
## Grafica Funcion
def graficaFuncion():
    plot(miFuncion, (x, -15, 15), (y, -15, 15),  title="Funcion f(x)")
#
## Grafica Derivada
def graficaDerivada():
    plot(derivada, (x, -15, 15), (y, -15, 15), title="Derivada f'(x)")
#
## Grafica Recta Normal
def graficaNormal():
    plot(normal, (x, -15, 15), (y, -15, 15),title="Recta Normal")
#
## Grafica Recta Tangente
def graficaTangente():
    plot(tangente, (x, -15, 15), (y, -15, 15), title="Recta Tangente")
#
## Grafica Funcion + Derivada
def graficaFuncionDerivada():
    plot(miFuncion, derivada, (x, -15, 15), (y, -15, 15), title="Funcion y Derivada")
#
## Grafica Funcion + Normal
def graficaFuncionNormal():
    plot(miFuncion, normal, (x, -15, 15), (y, -15, 15), title="Funcion y Recta Normal")
#
## Grafica Funcion + Tangente
def graficaFuncionTangente():
    plot(miFuncion, tangente, (x, -15, 15), (y, -15, 15), title="Funcion y Recta Tangente")
#
## Grafica Recta Normal + Tangente
def graficaNormalTangente():
    plot(tangente, normal, (x, -15, 15), (y, -15, 15), title="Recta Normal y Recta Tangente")
#
## Grafica Funcion + Normal + Tangente
def graficaFNormalTangente():
    plot(miFuncion, normal, tangente, (x, -15, 15), (y, -15, 15), title="Funcion, R.Normal y R.Tangente")
########################################
### Mostrar los resultados obtenidos ###
def derivarOUT():
    ## Limpiar Pantalla
    bienvenida.destroy()
    canvas = Canvas(width=700, height=520, bg="#FFFFCC") 
    ##Calculos Matematicos
    try:
        Sacar_Derivada() 
        Sacar_m()
        Obtener_y()
        obtener_recta_tangente()
        obtener_recta_normal()
    except:
        error()
    ######################################################################################
    ################################# Muestra Resultados #################################
    resultado = Tkinter.Label(root, text = "Resultado", font = "Century 12", justify = CENTER)
    resultado.config(bg = "#FFFFCC", fg = "black")
    resultado.place(x=300, y=15)
    #
    ## Muestra Funcion
    muestraFuncion0 = Tkinter.Label(root, text= "Funcion", font = "Courier 10", justify = CENTER)
    muestraFuncion0.config(bg = "#FFFFCC", fg = "black")
    muestraFuncion0.place(x = 60, y = 50)
    #
    muestraFuncion1 = Tkinter.Label(root, text= str(miFuncion), font = "Courier 10", justify = CENTER)
    muestraFuncion1.config(bg = "#FFFFCC", fg = "black")
    muestraFuncion1.place(x = 200, y = 50)
    #
    ## Muestra Punto
    muestraPunto0 = Tkinter.Label(root, text= "Punto", font = "Courier 10", justify = CENTER)
    muestraPunto0.config(bg = "#FFFFCC", fg = "black")
    muestraPunto0.place(x = 60, y = 75)
    #
    muestraPunto1 = Tkinter.Label(root, text= str(int(miPunto)), font = "Courier 10", justify = CENTER)
    muestraPunto1.config(bg = "#FFFFCC", fg = "black")
    muestraPunto1.place(x = 200, y = 75)
    #
    ## Muestra Derivada 
    muestraDerivada0 = Tkinter.Label(root, text= "Derivada", font = "Courier 10", justify = CENTER)
    muestraDerivada0.config(bg = "#FFFFCC", fg = "black")
    muestraDerivada0.place(x = 60, y = 100)
    #
    muestraDerivada1 = Tkinter.Label(root, text= str(derivada), font = "Courier 10", justify = CENTER)
    muestraDerivada1.config(bg = "#FFFFCC", fg = "black")
    muestraDerivada1.place(x = 200, y = 100)
    #
    ## Muestra RectaNormal
    rectaN = Tkinter.Label(root, text= "Recta Normal", font = "Courier 10", justify = CENTER)
    rectaN.config(bg = "#FFFFCC", fg = "black")
    rectaN.place(x = 60, y = 125)
    #
    rectaN = Tkinter.Label(root, text= str(normal), font = "Courier 10", justify = CENTER)
    rectaN.config(bg = "#FFFFCC", fg = "black")
    rectaN.place(x = 200, y = 125)
    #
    ## Muestra Recta Tangente
    rectaT = Tkinter.Label(root, text= "Recta Tangente", font = "Courier 10", justify = CENTER)
    rectaT.config(bg = "#FFFFCC", fg = "black")
    rectaT.place(x = 60, y = 150)
    #
    rectaT = Tkinter.Label(root, text= str(tangente), font = "Courier 10", justify = CENTER)
    rectaT.config(bg = "#FFFFCC", fg = "black")
    rectaT.place(x = 200, y = 150)
    ######################################################################################
    ###################################### Graficas ######################################
    resultado = Tkinter.Label(root, text = "Graficas", font = "Century 12", justify = CENTER)
    resultado.config(bg = "#FFFFCC", fg = "black")
    resultado.place(x=300, y=180)
    ## Boton Grafica Funcion
    bttn_funcion = Tkinter.Button(root, text= "Funcion ", relief = "raised",command = graficaFuncion)
    bttn_funcion.config(bg = "#CC99FF", fg = "black")
    bttn_funcion.place(x = 50, y = 200)
    #
    ## Boton Grafica Derivada
    bttn_derivada = Tkinter.Button(root, text= "Derivada", relief = "raised",command = graficaDerivada)
    bttn_derivada.config(bg = "#CC99FF", fg = "black")
    bttn_derivada.place(x = 150, y = 200)
    #
    ## Boton Grafica Recta Normal
    bttn_normal = Tkinter.Button(root, text= "Recta Normal", relief = "raised",command = graficaNormal)
    bttn_normal.config(bg = "#CC99FF", fg = "black")
    bttn_normal.place(x = 250, y = 200)
    #
    ## Boton Grafica Recta Tangente
    bttn_tangente = Tkinter.Button(root, text= "Recta Tangente", relief = "raised",command = graficaTangente)
    bttn_tangente.config(bg = "#CC99FF", fg = "black")
    bttn_tangente.place(x = 370, y = 200)
    #
    ## Boton Grafica Funcion + Normal + Tangente
    bttn_tangente = Tkinter.Button(root, text= "Funcion+Normal+Tangente", relief = "raised",command = graficaFNormalTangente)
    bttn_tangente.config(bg = "#CC99FF", fg = "black")
    bttn_tangente.place(x = 515, y = 250)
    #
    ## Boton Grafica Funcion + Derivada
    bttn_funcionDerivada = Tkinter.Button(root, text= "Funcion\nDerivada", relief = "raised",command = graficaFuncionDerivada)
    bttn_funcionDerivada.config(bg = "#CC99FF", fg = "black")
    bttn_funcionDerivada.place(x = 50, y = 250)
    #
    ## Boton Grafica Funcion + Normal
    bttn_funcionNormal = Tkinter.Button(root, text= "Funcion \nNormal", relief = "raised",command = graficaFuncionNormal)
    bttn_funcionNormal.config(bg = "#CC99FF", fg = "black")
    bttn_funcionNormal.place(x = 150, y = 250)
    #
    ## Boton Grafica Funcion + Tangente
    bttn_fTang = Tkinter.Button(root, text= "Funcion\n   Tangente   ", relief = "raised",command = graficaFuncionTangente)
    bttn_fTang.config(bg = "#CC99FF", fg = "black")
    bttn_fTang.place(x = 250, y = 250)
    #
    ## Boton Grafica Recta Normal + Tangente
    bttn_fnormalTang = Tkinter.Button(root, text= "Normal\n    Tangente    ", relief = "raised",command = graficaNormalTangente)
    bttn_fnormalTang.config(bg = "#CC99FF", fg = "black")
    bttn_fnormalTang.place(x = 370, y = 250)
    #
    ## Boton Nueva Derivada
    bttn_nuevaDer = Tkinter.Button(root, text= "\n     Nueva Derivada     \n", relief = "raised",command = derivarIN)
    bttn_nuevaDer.config(bg = "#CC99FF", fg = "black")
    bttn_nuevaDer.place(x = 300, y = 350)
    """ ====================== FIN derivaOUT ======================= """

########################################
###            Acerca de             ###
def acerca():
    """ Muestra un "acerca" del Programa"""
    ## Crea la ventana.
    global acerca_win
    acerca_win = Tkinter.Toplevel(root)
    acerca_win.geometry("200x200+200+200")
    acerca_win.config(bg = "#FFFFCC")
    acerca_win.transient(root)
    ## Informacion.
    acerca_win.title("Acerca de")
    lb_acerca  = Tkinter.Label(acerca_win, text = "Creado por\nSamantha Arburola\n&\nSamantha Mendoza\n&\nAmanda Solano")
    lb_acerca.config(bg = "#FFFFCC", fg = "black")
    lb_acerca.place(x=45, y=15)
    #
    lb_inst    = Tkinter.Label(acerca_win, text = "ITCR"+ "\n" +"Ing. en Computacion\nVersion 1.0")
    lb_inst.config(bg = "#FFFFCC", fg = "black")
    lb_inst.place(x=45, y=115)
    #
    lb_version = Tkinter.Label(acerca_win, text = "Version 1.0")
    lb_version.config(bg = "#FFFFCC", fg = "black")
    ## Boton Cerrar Ventana
    bttn_cerrar = Tkinter.Button(acerca_win, text= "Cerrar", relief = "raised",command = lambda:acerca_win.destroy())
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x=80,y=170)
    acerca_win.transient(root)
    """ ====================== FIN  AcercaDe ======================= """    
########################################
### Mostrar las instrucciones        ###
def instructivo():
    """ Explica el objetivo del juego """
    ## Crea la ventana.
    global instructivo
    instructivo = Tkinter.Toplevel(root)
    instructivo.config(bg = "#FFFFCC")
    instructivo.title("DeriPy - INSTRUCCIONES")
    instructivo.geometry("420x320+400+370") ## Ancho, Largo, X Y (posiciona ventana pantalla)
   
    
    ## Texto: Titulo.
    titulo = Tkinter.Label(instructivo, text = "INSTRUCCIONES" , font = "Impact 12")
    titulo.config(bg = "#FFFFCC", fg = "black")
    titulo.place(x = 20, y = 25)
    #
    instruccion0 = Tkinter.Label(instructivo, text= "Ingresar la funcion con los siguientes formatos", font = "Courier 8", justify = CENTER)
    instruccion0.config(bg = "#FFFFCC", fg = "black")
    instruccion0.place(x = 30, y = 50)
    #
    instruccion1 = Tkinter.Label(instructivo, text= "Suma           |  +\nResta          |  -\nDivicion       |  /\nMultiplicacion |  *\nFraccion       |  (numerador / denominador)", font = "Courier 8", justify = LEFT)
    instruccion1.config(bg = "#FFFFCC", fg = "black")
    instruccion1.place(x = 30, y = 65)
    #
    instruccion2 = Tkinter.Label(instructivo, text= "Exponencial    |  numero**exponente\nSeno           |  sin(x)\nCoseno         |  cos(x)\nTangente       |  tan(x)\nRaiz Cuadrada  |  sqrt(expresion)", font = "Courier 8", justify = LEFT)
    instruccion2.config(bg = "#FFFFCC", fg = "black")
    instruccion2.place(x = 30, y = 115)
    #
    instruccion3 = Tkinter.Label(instructivo, text= "USAR MINUSCULAS PARA ESCRIBIR LA FUNCION\n\nSe recomienda agrupar operaciones entre parentesis ()", font = "Courier 8", justify = CENTER)
    instruccion3.config(bg = "#FFFFCC", fg = "black")
    instruccion3.place(x = 30, y = 200)
    ## Boton Cerrar Ventana
    bttn_cerrar = Tkinter.Button(instructivo, text= "Cerrar", relief = "raised",command = lambda:instructivo.destroy())
    bttn_cerrar.config(bg = "#CC99FF", fg = "black")
    bttn_cerrar.place(x = 180, y = 280)
    instructivo.transient(root)
    """ ===================== FIN Instructivo ====================== """
""" ---------------------------------------------------------------- """
########################################################################
""" ---------------------------------------------------------------- """
""" ---------------------- Ventana Principal ----------------------- """
""" ---------------------------------------------------------------- """
## Funcion Auxiliar para salir del programa
def salir():    
    root.destroy()
    root.quit()
## Ventana principal y propiedades
root = Tk()
root.title("DeriPy")
root.geometry("720x520+150+150")
root.config(bg = "#FFFFCC")
## Barra de menu
menubar = Menu(root)
## SubMenu Derivar
filemenu = Menu(menubar, tearoff = 0)
filemenu.add_command(label = "Ingresar", command = derivarIN)
filemenu.add_command(label = "Cerrar", command = salir)
menubar.add_cascade(label  = "Derivar", menu = filemenu)
## SubMenu Ayuda
helpmenu = Menu(menubar, tearoff = 0)
helpmenu.add_command(label = "Intrucciones", command = instructivo)
helpmenu.add_command(label = "Acerca de...", command = acerca)
menubar.add_cascade(label  = "Ayuda", menu = helpmenu)
## Mensaje Bienvenida
global bienvenida
bienvenida = Tkinter.Label(root, text= " - ' ' ' - . . . - ' ' ' - . . . - \nBienvenido-a\n\nGraficador de Derivadas\n\nDeriPy\n - . . . - ' ' ' - . . . - ' ' ' - ", font = "Courier 20", justify = CENTER)
bienvenida.config(bg = "#FFFFCC", fg = "black")
bienvenida.place(x = 75, y = 150)
root.config(menu=menubar)


root.mainloop()
